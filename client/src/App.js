import React, {useEffect, useState} from 'react'
import {BrowserRouter, Switch, Route } from "react-router-dom"
import './App.css';
import Login from './components/auth/Login.js'
import Register from './components/auth/Register.js'
import FakeHeader from './components/misc/fakeHeader.js'
import Home from './components/misc/home'
import UserContext from './context/userContext'
import axios from 'axios'

function App() {
  const [userData, setUserData] = useState({
    token: undefined,
    user: undefined
  })
  // const [pleaselogin, setPleaseLogin] = useState(false)
  // check if logged in and update the userData state
  const checkLoggedIn = async () => {
    // get key named auth-token
    // localstoraeg contains key value pairs
    let token = localStorage.getItem("auth-token");
    // first time there wont be an auth-token key. it will be null, so it will cause a server 500 error.
    // if null we should set it to an empty string in local storage
    // then set the token above to empty string
    console.log('token: ', token)
    if (token === null) {
      localStorage.setItem("auth-token", "");
      token = ""
    }
    // we post to below endpoint the headers object with token
    const tokenRes = await axios.post(
      "/users/tokenIsValid",
      null,
      { headers: {"x-auth-token": token}}
    );
    console.log(tokenRes.data);
    if (tokenRes.data) {
      const userRes = await axios.get('/users', {headers: {"x-auth-token": token},
      });
      console.log('userres', userRes)
      setUserData({
        token,
        user: userRes.data
      })
    }
  }

  useEffect(() => {
    checkLoggedIn() 
    // setPleaseLogin(true)
  }, []);

  console.log('render')

  return (
    <div className="container">
      <BrowserRouter>
        <UserContext.Provider value={{ userData, setUserData }} >
        {/* context creates state it can share with other components */}
        {/* pass a value to it */}
        {/* <pre>{JSON.stringify(userData)}</pre> */}
          <FakeHeader />
          <Switch>
            <Route exact path="/" component={Home}></Route>
            <Route path="/login" component={Login}></Route>
            <Route path="/register" component={Register}></Route>

          </Switch>
        </UserContext.Provider>
      </BrowserRouter>
    </div>

  );
}

export default App;
