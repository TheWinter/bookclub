import React, {useEffect, useState, useContext} from 'react'
import axios from 'axios';
import UserContext from "../../context/userContext";
import AddTodo from './addTodo'
import TodoItem from './todoItem'
import { FaRegBell } from 'react-icons/fa';

function Todos() {
  const [todos, setTodos] = useState([]);

  const { userData } = useContext(UserContext);

  useEffect(() => {
    getTodos()  
    console.log("lets say we are fetching data")
  }, [])

  const getTodos = async () => {
    const res = await axios.get(`/todos/all`, { headers: { 'x-auth-token': userData.token }})
    const data = await res.data;
    setTodos(data)
  }

  const createTodo = async (title) => {
    const res = await axios.post('/todos', {title: title}, { headers: { 'x-auth-token': userData.token }})
    setTodos(res.data)
  }

  // original handler for editing a todo
  const updateTodo = async (id, newValue) => {
    // this is receiving the updated value
    const res = await axios.post('/todos/update/'+id, {title: newValue}, { headers: { 'x-auth-token': userData.token }})
    const data = await res.data;
    setTodos(data)
  }

  const removeTodo = async (id) => {
    console.log('rem id: ', id)
    const res = await axios.delete('/todos/'+id, {
       headers: { 'x-auth-token': userData.token }})
    setTodos(res.data)
  }


  return (
    <div>
        <h3 className="vibes section-heading"><FaRegBell/> Club Notifications</h3>
        {userData.user ? (
            // userdata user true block begin
            <div >
                { todos ? todos.map(todo => (
                  <TodoItem key={todo._id} todo={todo} removeTodo={removeTodo} updateTodo={updateTodo}/>  
                )) 
                : <p> End of First Block</p>}
            <AddTodo onSubmit={createTodo}/>
            </div>
            // userdata user true block end
        ) : (
           <p> You are not logged in!!! </p> 
        )}
    </div>
  )
}

export default Todos;
