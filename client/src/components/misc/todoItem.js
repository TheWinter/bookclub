import React, {useEffect, useState} from 'react'
import AddTodo from './addTodo'
import { MdEdit } from 'react-icons/md'
import { RiDeleteBin6Line } from 'react-icons/ri'

const TodoItem = ({todo, removeTodo, updateTodo}) => {
    const [edit, setEdit] = useState({
        id: null, 
        value: ''
    })

    const submitUpdate = value => {
        console.log('updated val: ', value)
        updateTodo(edit.id, value)
        setEdit({
            id: null,
            value: ''
        })
    }

    const handleCancel = () => {
        setEdit({
            id: null,
            value: ''
        })
    }

    // if theres an edit.id
    // return this component and pass props to it
    if (edit.id) {
        console.log('edir: ', edit)
        // return <EditTodo edit={edit} onSubmit={submitUpdate} />
        return <AddTodo edit={edit} handleCancel={handleCancel} onSubmit={submitUpdate}/>
    }
     
return(
    <div className="todo-container">
        <div>
            <p>{todo.title}</p>
        </div>
        <div>
            <div onClick={() => {if(window.confirm('Delete Notification?'))removeTodo(todo._id)}}>< RiDeleteBin6Line /></div>
            <div onClick={() => setEdit({ id: todo._id, value: todo.title})}><MdEdit/></div>
        </div>
    </div> 
    ) 
}

export default TodoItem