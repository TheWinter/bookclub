import React, {useEffect, useState} from 'react'
import Moment from 'react-moment';
import 'moment-timezone';
import AddEvent from './addEvent'
import {TiTick} from 'react-icons/ti'
import { RiDeleteBin6Line } from 'react-icons/ri'
import { MdEdit } from 'react-icons/md'


const EventItem = ({event, removeEvent, updateEvent, confirmParticipate, declineParticipate, confirmed, declined}) => {
    const [edit, setEdit] = useState({
        id: null, 
        title: '',
        where: ''
    })
    // useEffect(() => { 
    //     console.log("item event: ", event, confirmed)
    //   }, [])
      

    const submitUpdate = (title, where) => {
        console.log('hello: ', title, where)
        updateEvent(edit.id, title, where)
        setEdit({
            id: null,
            title: '',
            where: ''
        })
    }

    const handleCancel = () => {
        setEdit({
            id: null,
            title: '',
            where: ''
        })
    }

    if (edit.id) {
        console.log('edir: ', edit)
        // return <EditTodo edit={edit} onSubmit={submitUpdate} />
        return <AddEvent edit={edit} handleCancel={handleCancel} onSubmit={submitUpdate}/>
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        // creates the notification
        // TODO for update should change value of note
        confirmParticipate(event._id)
        console.log('sumbit handled: ', event._id)
    }

    const handleDecline = (e) => {
        e.preventDefault()
        // creates the notification
        // TODO for update should change value of note
        declineParticipate(event._id)
        console.log('decline handled: ', event._id)
    }
 
return(
    <div className="event-item">
        <div>
            <h5>{event.title}</h5>
            <div className="title-and-text">
                <p><b>Location:&nbsp;</b></p>
                <p>{event.where}</p> 
            </div>
            <div className="title-and-text">       
                <p><b>Date:&nbsp;</b></p>
                <Moment format='DD-MM-YYYY'>{event.date}</Moment>
            </div>
            {confirmed.length > 0 ? 
            <div>
                <h6 className="yes">Participating! <TiTick/></h6>
                <p>Look forward to seeing you!</p> 
            </div>
            : declined.length > 0 ?
            <div>
                <h6 className="no">Not Participating!</h6>
            </div>
            :
            <div>
                <p>Can you participate?</p>
                <form onSubmit={handleSubmit}>
                    <input className="hidden" id="theid" name="theid" value={event._id} />
                    <button>Yes</button>
                </form>
                <form onSubmit={handleDecline}>
                    <input className="hidden" id="theid" name="theid" value={event._id} />
                    <button>No</button>
                </form>
            </div>
            }
        </div>
        <div>
            <div onClick={() => {if(window.confirm('Delete Event?'))removeEvent(event._id)}}>< RiDeleteBin6Line /></div>
            <div onClick={() => setEdit({ id: event._id, title: event.title, where: event.where})}><MdEdit/></div>
        </div>
    </div> 
    ) 
}

export default EventItem