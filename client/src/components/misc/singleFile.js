import React, {useEffect} from 'react'

const SingleFile = ({file, removeFile}) => {

    return (
        <div className="file-section">
           <p><a href={'/uploads/'+file}>{file}</a></p> 
           <div onClick={() => removeFile(file)}>x</div>
        </div>
    )
}

export default SingleFile
