import axios from 'axios'
import React, {Fragment, useState, useEffect, useContext} from 'react'
import UserContext from "../../context/userContext";
import { useForm } from 'react-hook-form'
import './misc.css';
import {BsBook} from 'react-icons/bs'
import SingleFile from './singleFile'


const FileUpload = () => {
    const { register, handleSubmit } = useForm()
    const [allFiles, setAllFiles] = useState(['name'])
    const [uploadedFile, setUploadedFile] = useState('')

    const { userData } = useContext(UserContext);

    useEffect(() => {
        getUploadsDir()  
        console.log("fetching uploads dir")
    }, [uploadedFile])

    const getUploadsDir = async () => {
        const res = await axios.get(`/api/uploads`, {headers: { 'x-auth-token': userData.token }})
        const data = await res.data;
        // turn back to json obj
        const jsdata = JSON.parse(data)
        setAllFiles(jsdata)
        console.log(jsdata)
    }

    const onSubmit = async (data, e) => {
      e.preventDefault()
      const formData = new FormData()
      for (let i=0; i < data.file.length; i++) {
          formData.append("file", data.file[i])
      } 
      console.log('data: ', formData)

      const res = await axios.post('/upload', formData, {
         headers: { 'x-auth-token': userData.token }}
    )
      console.log('upload res: ', res)
      const {fileName, filePath} = res.data
      setUploadedFile(res.data.message)
    }

    const removeFile = async (filename) => {
        console.log('rem id: ', filename)
        const res = await axios.delete('/upload/'+filename, {headers: { 'x-auth-token': userData.token }})
        console.log(res.data)
        setUploadedFile(res.data.message)
      }

    return (
        <Fragment>
            <form onSubmit={handleSubmit(onSubmit)}>
                <input ref={register} type="file" name="file[]" multiple />
                <button>Submit</button>
            </form>
            {/* {uploadedFile ? <div className="row mt-5">
                <div className="col-md-6 m-auto">
                    <h3 className="text-center">
                        {uploadedFile.fileName}
                    </h3>
                </div>
            </div> : null} */}
            <h3 className="vibes section-heading"><BsBook /> Library</h3>
            <div className="library">
                {allFiles ? allFiles.map(file => (
                    // <p><a href={'/uploads/'+file}>{file}</a></p>
                    <SingleFile key={file} file={file} removeFile={removeFile} />
                )) : null}
            </div>
        </Fragment>

    )
}

export default FileUpload