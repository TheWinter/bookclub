import React, {useRef, useState, useEffect} from 'react'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import './misc.css';

const AddEvent = (props) => {
    console.log('props: ', props)
    const [upTitle, setupTitle] = useState(props.edit ? props.edit.title : '');
    const [upWhere, setupWhere] = useState(props.edit ? props.edit.where : '');

    const [startDate, setStartDate] = useState(new Date());

    const title = useRef('')
    const where = useRef('')

    const handleChangeT = e => {
        setupTitle(e.target.value);
      };

    const handleChangeW = e => {
        setupWhere(e.target.value);
    };

    const handleSubmit = (e) => {
        e.preventDefault()

        console.log(title.current.value)

        // creates the event
        props.onSubmit(title.current.value, where.current.value, startDate)
        title.current.value = ''
        where.current.value = ''
    }

    const cancelEdit = (e) => {
        e.preventDefault()
        props.handleCancel()
    }

    return(
        <div className={props.edit ? "event-item" : "noteInput"}>
            <form onSubmit={handleSubmit}>
            {props.edit ? (
                <>
                    <h6>Editing Event</h6>
                    <label>Event Name:</label>
                    <br/>
                    <input type="text" ref={title} value={upTitle} onChange={handleChangeT} required></input>
                    <br/>
                    <label>Location:</label>
                    <br/>
                    <input type="text" ref={where} value={upWhere} onChange={handleChangeW} required></input>
                    {/* <br/>
                    <label>Date:</label>
                    <br/>
                    <DatePicker selected={startDate} onChange={date => setStartDate(date)} />
                   */}
                    <br/> 
                    <input type="submit" value="Update"/>
                    <button onClick={(e) => cancelEdit(e)}>Cancel</button>
                </>
            ) : (
                <>
                    <label>Event Name:</label>
                    <br/>
                    <input type="text" ref={title} required></input>
                    <br/>
                    <label>Location:</label>
                    <br/>
                    <input type="text" ref={where} required></input>
                    <br/>
                    <label>Date:</label>
                    <br/>
                    <DatePicker selected={startDate} onChange={date => setStartDate(date)} />
                    <br/>
                    <input type="submit"/>
                </>
            )}
            </form>
        </div>
    )
}

export default AddEvent