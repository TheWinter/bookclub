import React, {useRef, useState, useEffect} from 'react'
import './misc.css';

const AddTodo = (props) => {
    console.log('props: ', props)
    const [input, setInput] = useState(props.edit ? props.edit.value : '');

    const title = useRef('')

    const handleChange = e => {
        setInput(e.target.value);
      };

    
    useEffect(() => {
        title.current.focus();
    });

    const handleSubmit = (e) => {
        e.preventDefault()

        console.log(title.current.value)

        // creates the notification
        // TODO for update should change value of note
        props.onSubmit(title.current.value)
        title.current.value = ''
    }

    const cancelEdit = (e) => {
        e.preventDefault()
        props.handleCancel()
    }

    return(
        <div className={props.edit ? "todo-container" : "noteInput"}>
            <form onSubmit={handleSubmit}>
            {props.edit ? (
                <>
                    <h6>Editing Notification</h6>
                    <textarea ref={title} value={input} onChange={handleChange} required></textarea>
                    {/* <input type="text" ref={clName}></input> */}
                    <button onClick={(e) => cancelEdit(e)}>Cancel</button>
                    <input type="submit" value="Update" />
                </>
            ) : (
                <>
                    <textarea ref={title} required></textarea>
                    {/* <input type="text" ref={clName}></input> */}
                    <input type="submit"/>
                </>
            )}
            </form>  
        </div>
    )
}

export default AddTodo