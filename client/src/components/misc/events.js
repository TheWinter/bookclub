import React, {useEffect, useState, useContext} from 'react'
import axios from 'axios';
import UserContext from "../../context/userContext";
import AddEvent from './addEvent'
import EventItem from './eventItem'
import {AiTwotoneCalendar} from 'react-icons/ai'

function Events() {
  const [events, setEvents] = useState([]);

  const { userData } = useContext(UserContext);

  useEffect(() => {
    getEvents()  
    console.log("lets say we are fetching events")
  }, [])

  const getEvents = async () => {
    const res = await axios.get(`/events/all`, { headers: { 'x-auth-token': userData.token }})
    const data = await res.data;
    setEvents(data)
    console.log('fe events: ', events)
  }

  const createEvent = async (title, where, date) => {
    const res = await axios.post('/events', {title: title, where: where, date: date}, { headers: { 'x-auth-token': userData.token }})
    setEvents(res.data)
  }

  // comfirm participation of event
  const confirmParticipate = async (theid) => {
    const res = await axios.post('/events/confirm', {theid: theid}, { headers: { 'x-auth-token': userData.token }})
    setEvents(res.data)
  }

  // decline participation of event
  const declineParticipate = async (theid) => {
    const res = await axios.post('/events/decline', {theid: theid}, { headers: { 'x-auth-token': userData.token }})
    setEvents(res.data)
  }


  // original handler for editing a todo
  const updateEvent = async (id, newTitle, newWhere) => {
    // this is receiving the updated value
    const res = await axios.post('/events/update/'+id, {title: newTitle, where: newWhere}, { headers: { 'x-auth-token': userData.token }})
    const data = await res.data;
    setEvents(data)
  }


  const removeEvent = async (id) => {
    console.log('rem id: ', id)
    const res = await axios.delete('/events/'+id, {
       headers: { 'x-auth-token': userData.token }})
    setEvents(res.data)
  }


  return (
    <div>
        <h3 className="vibes section-heading"><AiTwotoneCalendar /> Club Events</h3>
        {userData.user ? (
            // userdata user true block begin
            <div >
                { events ? events.map(event => (
                  <EventItem key={event._id} event={event} removeEvent={removeEvent} updateEvent={updateEvent} confirmParticipate={confirmParticipate} declineParticipate={declineParticipate} confirmed={event.participants} declined={event.declines} /> 
                  )) 
                : <p> End of First Block</p>}
            <AddEvent onSubmit={createEvent}/>
            </div>
            // userdata user true block end
        ) : (
           <p> You are not logged in!!! </p> 
        )}
    </div>
  )
}

export default Events;
