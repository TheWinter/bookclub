import React, { useContext, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import UserContext from "../../context/userContext";
import Todos from './todos'
import FileUpload from './fileUpload'
import Events from './events'


export default function Home() {
  const { userData } = useContext(UserContext);
  const history = useHistory()

  // removed dependency array so that when you logout, it will also redirect you to login page
  // without removing dep array, logging out does not redirect you to login page (stays on home page)
  useEffect(() => {
      if (!userData.user) history.push("/login")
  }, []);

  return (
    <div className="page">
      {userData.user ? (
        <div>
            <p>Welcome {userData.user.displayName}</p>
                <div className="row">
                    <div className="col-lg-6 col-md-12 col-sm-12">
                        <Todos />
                        <Events />
                    </div>
                    <div className="col-lg-6 col-md-12 col-sm-12">
                        <FileUpload />
                    </div>
                </div>
        </div>
      ) : (
        <>
          <h2>You are not logged in</h2>
          <Link to="/login">Log in</Link>
        </>
      )}
    </div>
  );
}