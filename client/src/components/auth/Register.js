import React, {useState, useContext} from 'react'
import { useHistory } from 'react-router-dom'
import UserContext from "../../context/userContext";
import Error from '../misc/Error.js'
import axios from 'axios';

import './auth.css'

export default function Register(props) {
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [passwordCheck, setPasswordCheck] = useState();
    const [displayName, setDisplayName] = useState();
    const [error, setError] = useState();
  
    const { setUserData } = useContext(UserContext);
    const history = useHistory();

    const submit = async (e) => {
        e.preventDefault();
        try {
            // getting stuff from state
            const newuser = {email, password, passwordCheck, displayName};
            // posting new registeree to the backend route
            await axios.post(
                "/users/register",
                newuser
            )
            // logging in the successful registeree
            const loginRes = await axios.post("/users/login", {email, password})
            // putting their details into the usercontext?
            setUserData({
                token: loginRes.data.token,
                user: loginRes.data.user
            })
            // setting their token in localstorage
            localStorage.setItem("auth-token", loginRes.data.token)
            // takes us to home page?
            history.push("/")
        } catch (err) {
            err.response.data.msg && setError(err.response.data.msg);
          }
    }


    return (
        <div className="page">
        <h2>Register</h2>
        {error && (
          <Error message={error} clearError={() => setError(undefined)} />
        )}
        <form className="form" onSubmit={submit} >
          <label htmlFor="register-email">Email</label>
          <input
            id="register-email"
            type="email"
            onChange={(e) => setEmail(e.target.value)}
          />
  
          <label htmlFor="register-password">Password</label>
          <input
            id="register-password"
            type="password"
            onChange={(e) => setPassword(e.target.value)}
          />
          <input
            type="password"
            placeholder="Verify password"
            onChange={(e) => setPasswordCheck(e.target.value)}
          />
  
          <label htmlFor="register-display-name">Display name</label>
          <input
            id="register-display-name"
            type="text"
            onChange={(e) => setDisplayName(e.target.value)}
          />
  
          <input type="submit" value="Register" />
        </form>
      </div>
    )
}

