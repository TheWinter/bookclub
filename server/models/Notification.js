let mongoose = require('mongoose');
const Schema = mongoose.Schema;
// const User = require('./User.js').User;

const NotificationSchema = new mongoose.Schema({
    _id: Schema.Types.ObjectId,
    note: String,
    // likes: [{ type: Schema.Types.ObjectId, ref: 'User' }],
    date: { type: Date, default: Date.now },
});

const Notification = mongoose.model("Notification", NotificationSchema);


module.exports = Notification;