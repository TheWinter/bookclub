let mongoose = require('mongoose');
const Schema = mongoose.Schema;
// const User = require('./User.js').User;

const eventModel = new mongoose.Schema({
    title: { type: String, required: true },
    date: { type: Date, default: Date.now },
    where: { type: String, required: true },
    participants: [{ type: Schema.Types.ObjectId, ref: 'User' }],
    declines: [{ type: Schema.Types.ObjectId, ref: 'User' }],
    cancellations: [{ type: Schema.Types.ObjectId, ref: 'User' }]
});

const Event = mongoose.model("event", eventModel);


module.exports = Event;