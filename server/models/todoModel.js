let mongoose = require('mongoose');
const Schema = mongoose.Schema;
// const User = require('./User.js').User;

const todoSchema = new mongoose.Schema({
    title: { type: String, required: true },
    userId: { type: String, required: true },
    date: { type: Date, default: Date.now }
});

const Todo = mongoose.model("todo", todoSchema);


module.exports = Todo;