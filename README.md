### Description
This is a social platform for a group like a book club or other organisation to use to share notifications, events, and documents.
It is a React version of a project I made previously (see "Group Space" repo), which is an unbranded version of a website currently in use by a community choir. 

### Getting Started

Clone repo
```
git clone <repository>
```
- Create an uploads folder in the public directory - this is where the files will be uploaded via the file uploader feature
- Create a config folder in the client directory, and add a file named db.js. This is where your database connection string will be defined. Template below 
```
module.exports = {
    db_dev: '<connection string here>'
}
```

Command to run client & server
```
npm run dev
```

### Features

- Front end built with React, uses React Hooks
- Uses Bootstrap styling framework and is responsive
- Uses Axios to post and get from backend routes
- Backend built with Node js, using Express framework
- MongoDB and Mongoose 
- Uses JWT authentication method

### Photos

![logged in view](./client/public/img/logged-in.png)




