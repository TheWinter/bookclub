const express = require('express')
const cors = require('cors')
const { v4: uuidv4 } = require('uuid');
const fs = require('fs');
const path = require('path');
require("dotenv").config();
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const mongoose = require('mongoose')
const db = require('./client/config/db').db_dev;
const fileUpload = require('express-fileupload')
const User = require("./server/models/User")
const Todo = require("./server/models/todoModel")
const Event = require("./server/models/eventModel")
const auth = require("./middleware/auth")

const Notification = require('./server/models/Notification.js')

const app = express()

mongoose.connect(db, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true})
    .then(() => console.log("MongoDB connected"))
    .catch(err => console.log(err))
// body parser

app.use(express.json({extended: false}))
app.use(cors())
app.use(fileUpload())

// uploads dirname
const directoryPath = path.join(__dirname, '/client/public/uploads');

// uploads get
app.get('/api/uploads', auth, (req, res) => {
    var files = fs.readdirSync(directoryPath)
    var jsonFiles = JSON.stringify(files);
    res.json(jsonFiles)
})

app.post("/upload", auth, async (req, res) => {
    try {
      if(!req.files){
        res.send({
          status: false,
          message: "No files"
        })
      } else {
        const { file } = req.files
        
        if (file instanceof Array) {
            for (let i=0; i < file.length; i++) {
                file[i].mv(__dirname+"/client/public/uploads/" + file[i].name)
            }
        } else {
            file.mv(__dirname+"/client/public/uploads/" + file.name)
        }
  
        res.send({ 
            uploaded: true,
            message: "files were uploaded" 
        })
      }
    } catch (e) {
      console.log(e)
    }
  })

  // delete an upload
  app.delete('/upload/:filename', auth, (req, res) => {
    // const filename = sanitize(req.params.filename);
    const { filename } = req.params
    fs.unlink(__dirname+"/client/public/uploads/"+filename, (err) => {
        if (err) console.log(err);
        res.send({ 
            uploaded: true,
            message: "files were deleted" 
        })
    });
});

// Registration
app.post('/users/register', async (req, res) => {
    try {
        let {email, password, passwordCheck, displayName} = req.body;

        if (!email || !password || !passwordCheck) 
            return res.status(400).json({msg: 'Not all fields have been entered'}); 
        
        if (password.length < 5)
            return res.status(400).json({msg: 'The password needs to be at least 5 characters long'}); 
        if (password != passwordCheck)
            return res
                .status(400)
                .json({msg: 'Enter the same password twice for verification'})
        
        const existingUser = await User.findOne({email: email})
        if (existingUser)
            return res
                .status(400)
                .json({msg: "An account with this email already exists"})

        genDispName = email.split('@')[0]
        if (!displayName) displayName = genDispName

        const salt = await bcrypt.genSalt();
        const passwordHash = await bcrypt.hash(password, salt)

        const newUser = new User ({
            email,
            password: passwordHash,
            displayName
        })

        const savedUser = await newUser.save()
        res.json(savedUser)
    } catch (err) {
        res.status(500).json({ error: err.message })
    }

})

app.post('/users/login', async (req, res) => {
    try {
        const { email, password } = req.body;

        // validate
        if (!email || !password) {
            return res.status(400).json({msg: 'Not all fields have been entered'}); 
        }
        const user = await User.findOne({email: email})
        if (!user)
            return res.status(400).json({msg: 'No account with this email has been registered'}); 

        const isMatch = await bcrypt.compare(password, user.password);
        if (!isMatch) return res.status(400).json({ msg: "Invalid credentials" })

        const token = jwt.sign({ id: user._id }, process.env.JWT_SECRET)
        // console.log(token)
        res.json({
            token,
            user: {
                id: user._id,
                displayName: user.displayName 
            },
        })
    } catch (error) {
        res.status(500).json({ err: error.message })
    }
})

app.delete("/delete", auth, async (req, res) => {
    console.log(req.user)
    try {
        const deletedUser = await User.findByIdAndDelete(req.user);
        res.json(deletedUser)
    } catch (err) {
        res.status(500).json({ error: err.message })
    }
})

app.post("/users/tokenIsValid", async (req, res) => {
    try {
      const token = req.header("x-auth-token");
      if (!token) return res.json(false);
  
      const verified = jwt.verify(token, process.env.JWT_SECRET);
      if (!verified) return res.json(false);
  
      const user = await User.findById(verified.id);
      if (!user) return res.json(false);
  
      return res.json(true);
    } catch (err) {
      res.status(500).json({ error: err.message });
    }
  });

app.get("/users", auth, async (req, res) => {
    const user = await User.findById(req.user)
    res.json({
        displayName: user.displayName,
        id: user._id
    });
})

// TODO
app.post("/todos", auth, async (req, res) => {
    try {
      const { title } = req.body;
  
      // validation
      if (!title)
        return res.status(400).json({ msg: "Not all fields have been entered yo." });
  
      const newTodo = new Todo({
        title,
        userId: req.user,
      });
      const savedTodo = await newTodo.save();
    //   res.json(savedTodo);
      res.redirect('/todos/all')
    } catch (err) {
      res.status(500).json({ error: err.message });
    }
  });

  app.delete('/todos/:id', auth, async (req, res) => {
    try {
        const trimmedId = req.params.id.trim()
        const todo = await Todo.findOne({ userId: req.user, _id: trimmedId });
        if (!todo)
          return res.status(400).json({
            msg: "No todo found with this ID that belongs to the current user.",
          });
        const deletedTodo = await Todo.findByIdAndDelete(trimmedId);
        // res.redirect('/todos/all')
        console.log('done!')
        const todos = await Todo.find({ userId: req.user });
        res.json(todos)
    } catch (err) {
        console.log('delete error: ', err)
    }
  });

// app.use("/todos", require("./routes/todoRouter"))
  app.get('/todos/all', auth, async (req, res) => {
      const todos = await Todo.find({ userId: req.user });
      res.json(todos)
  })

  app.post('/todos/update/:id', auth, async (req, res) => {
      try {
          const id = req.params.id;
          const { title } = req.body;      
          const todo = await Todo.findOne({ _id: id })
          todo.title = title;
      
          await todo.save();
          res.redirect('/todos/all')
      } catch (err) {
          console.log('update err: ', err)
      }
})


  /* Events */
// create new event
  app.post("/events", auth, async (req, res) => {
    try {
      const { title, where, date } = req.body;
  
      // validation
      if (!title || !where)
        return res.status(400).json({ msg: "Not all fields have been entered yo." });
  
      const newEvent = new Event({
        title,
        where,
        date
      });
      const savedEvent = await newEvent.save();
    //   res.json(savedTodo);
      res.redirect('/events/all')
    } catch (err) {
      res.status(500).json({ error: err.message });
    }
  });


  // get all events
  app.get('/events/all', auth, async (req, res) => {
    const events = await Event.find({}).sort({date: 1});
    res.json(events)
})

app.delete('/events/:id', auth, async (req, res) => {
    try {
        const { id } = req.params
        const event = await Event.findOne({ _id: id });
        if (!event)
          return res.status(400).json({
            msg: "No tevent found with this id.",
          });
        await Event.findByIdAndDelete(id);
        // res.redirect('/todos/all')
        console.log('done!')
        const events = await Event.find({});
        res.json(events)
    } catch (err) {
        console.log('delete error: ', err)
    }
  });

  app.post('/events/update/:id', auth, async (req, res) => {
    try {
        const id = req.params.id;
        const { title, where } = req.body;      
        const event = await Event.findOne({ _id: id })
        event.title = title;
        event.where = where;
    
        await event.save();
        res.redirect('/events/all')
      //   const todos = await Todo.find({ userId: req.user });
      //   res.json(todos)
    } catch (err) {
        console.log('update err: ', err)
    }
})

function removeA(arr) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax = arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}


// confirm participation
app.post('/events/confirm', auth, async (req, res) => {

    try {
        const { theid } = req.body;
    
        const event = await Event.findOne({_id: theid})
    
        var declineFound = event.declines.filter(decline => {
            // console.log("comparison", decline._id, user._id)
            return decline._id.toString() === req.user.toString();
        });
    
        if (declineFound.length > 0) {
            removeA(event.declines, declineFound[0])
            event.participants.push(req.user)
        } else {
            event.participants.push(req.user)
        }
    
        await event.save()
        res.redirect('/events/all')
    } catch (err) {
      console.log(err.message)
    }

})

// decline participation
app.post('/events/decline', auth, async (req, res) => {

    try {
        const { theid } = req.body;
    
        const event = await Event.findOne({_id: theid})
    
        var confirmFound = event.participants.filter(participant => {
            // console.log("comparison", decline._id, user._id)
            return participant._id.toString() === req.user.toString();
        });
    
        if (confirmFound.length > 0) {
            removeA(event.participants, confirmFound[0])
            event.declines.push(req.user)
        } else {
            event.declines.push(req.user)
        }
    
        await event.save()
        res.redirect('/events/all')
    } catch (err) {
      console.log(err.message)
    }

})



const port = 5000; 

app.listen(port, () => console.log(`server started on port ${port}`))